﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReqManager.Parsers
{
    public class ParsingManager
    {
        private const string FUNC_REQS_TITLE = "Функціональні вимоги";
        private const string NON_FUNC_REQS_TITLE = "Нефункціональні вимоги";
        private enum ReqType
        {
            FUNC,
            NONFUNC
        };
        private Dictionary<int, ReqType> titlesEntries = new Dictionary<int, ReqType>();
        public Tuple<List<string>, List<string>> GetRequirements(string filePath)
        {
            var functionalReqs = new List<string>();
            var nonFunctionalReqs = new List<string>();

            System.IO.TextReader reader = new EPocalipse.IFilter.FilterReader(filePath);
            var allTextFromFile = reader.ReadToEnd();
            int currentFuncIndex = 0;
            int currentNonFuncIndex = 0;
            int smallerIndex = 0;
            do
            {
                currentFuncIndex = FindStringEntry(allTextFromFile, FUNC_REQS_TITLE, smallerIndex);
                currentNonFuncIndex = FindStringEntry(allTextFromFile, NON_FUNC_REQS_TITLE, smallerIndex);

                smallerIndex = Math.Min(currentNonFuncIndex, currentFuncIndex);

                if (smallerIndex != -1)
                {

                    if (smallerIndex == currentFuncIndex)
                    {
                        titlesEntries.Add(smallerIndex, ReqType.FUNC);
                        smallerIndex = currentFuncIndex + 1;
                    }
                    else
                    {
                        titlesEntries.Add(smallerIndex, ReqType.NONFUNC);
                        smallerIndex = currentNonFuncIndex + 3;
                    }
                }
                else if (currentNonFuncIndex != -1)
                {
                    titlesEntries.Add(currentNonFuncIndex, ReqType.NONFUNC);
                    smallerIndex = currentNonFuncIndex + 3;
                }
                else if (currentFuncIndex != -1)
                {
                    titlesEntries.Add(currentFuncIndex, ReqType.FUNC);
                    smallerIndex = currentFuncIndex + 1;
                }

            }
            while (currentFuncIndex != -1 || currentNonFuncIndex != -1);

            for(int i=0;i<titlesEntries.Count;i++)
            {
                if (i == titlesEntries.Count-1)
                {
                    var length = allTextFromFile.Length - titlesEntries.ElementAt(i).Key;
                    var currentBlock = allTextFromFile.Substring(titlesEntries.ElementAt(i).Key, length);

                    int startPos = 0;
                    int reqEntry = -1;
                    int dotEntry = -1;

                    do
                    {
                        reqEntry = FindStringEntry(currentBlock, "REQ", startPos);
                        if (reqEntry != -1)
                        {
                            dotEntry = FindStringEntry(currentBlock, ".", reqEntry);
                            if (dotEntry != -1)
                            {
                                var length2 = dotEntry - reqEntry;
                                var requirement = currentBlock.Substring(reqEntry, length2);
                                if (titlesEntries.ElementAt(i).Value == ReqType.FUNC)
                                {
                                    functionalReqs.Add(requirement);
                                }
                                else
                                {
                                    nonFunctionalReqs.Add(requirement);
                                }
                                startPos = reqEntry + 3;
                            }
                        }
                    }
                    while (reqEntry != -1);

                    
                }
                else {
                    var length = titlesEntries.ElementAt(i+1).Key - titlesEntries.ElementAt(i).Key;
                    var currentBlock = allTextFromFile.Substring(titlesEntries.ElementAt(i).Key, length);

                    int startPos = 0;
                    int reqEntry = -1;
                    int dotEntry = -1;

                    do
                    {
                        reqEntry = FindStringEntry(currentBlock, "REQ", startPos);
                        if (reqEntry != -1)
                        {
                            dotEntry = FindStringEntry(currentBlock, ".", reqEntry);
                            if (dotEntry != -1)
                            {
                                var length2 = dotEntry - reqEntry;
                                var requirement = currentBlock.Substring(reqEntry, length2);
                                if (titlesEntries.ElementAt(i).Value == ReqType.FUNC)
                                {
                                    functionalReqs.Add(requirement);
                                }
                                else
                                {
                                    nonFunctionalReqs.Add(requirement);
                                }
                                startPos = reqEntry + 3;
                            }
                        }
                    }
                    while (reqEntry != -1);
                }
            }

            for(int i=0;i<functionalReqs.Count;i++)
            {
                functionalReqs[i] = functionalReqs[i].Substring(7, functionalReqs[i].Length - 7);
            }

            for (int i = 0; i < nonFunctionalReqs.Count; i++)
            {
                nonFunctionalReqs[i] = nonFunctionalReqs[i].Substring(7, nonFunctionalReqs[i].Length - 7);
            }

            reader.Close();
            return Tuple.Create(functionalReqs, nonFunctionalReqs);
        }

        private int FindStringEntry(string data, string termToSearch, int startIndex = 0)
        {
            return data.IndexOf(termToSearch, startIndex);
        }
    }
}