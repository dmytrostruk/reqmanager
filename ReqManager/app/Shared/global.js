"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Global = (function () {
    function Global() {
    }
    return Global;
}());
Global.BASE_REQ_ENDPOINT = 'api/requirements/';
Global.BASE_REQ_RESULT_ENDPOINT = 'api/requirements/result';
Global.BASE_REQ_UPLOAD_ENDPOINT = 'api/requirements/uploadfile';
exports.Global = Global;
//# sourceMappingURL=global.js.map