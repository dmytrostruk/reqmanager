﻿export class Global {
    public static BASE_REQ_ENDPOINT = 'api/requirements/';
    public static BASE_REQ_RESULT_ENDPOINT = 'api/requirements/result';
    public static BASE_REQ_UPLOAD_ENDPOINT = 'api/requirements/uploadfile';
}