﻿import { Component } from "@angular/core"
@Component({
    selector: "user-app",
    template: `<router-outlet></router-outlet>`
})

export class AppComponent {

}