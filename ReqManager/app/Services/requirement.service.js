"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
var global_1 = require("../Shared/global");
require("rxjs/add/operator/map");
require("rxjs/add/operator/do");
require("rxjs/add/operator/catch");
var RequirementService = (function () {
    function RequirementService(_http) {
        this._http = _http;
    }
    RequirementService.prototype.getRequirements = function () {
        return this._http.get(global_1.Global.BASE_REQ_ENDPOINT)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    RequirementService.prototype.getResults = function (answers) {
        var body = JSON.stringify(answers);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.post(global_1.Global.BASE_REQ_RESULT_ENDPOINT, body, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    RequirementService.prototype.uploadFile = function (formData) {
        return this._http.post(global_1.Global.BASE_REQ_UPLOAD_ENDPOINT, formData)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    RequirementService.prototype.handleError = function (error) {
        console.error(error);
        return Observable_1.Observable.throw(error.json().error || 'Server error');
    };
    return RequirementService;
}());
RequirementService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], RequirementService);
exports.RequirementService = RequirementService;
//# sourceMappingURL=requirement.service.js.map