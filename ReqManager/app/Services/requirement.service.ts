﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { Global } from '../Shared/global';
import { Answer } from "../Models/answer";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

@Injectable()
export class RequirementService {
    constructor(private _http: Http) { }

    getRequirements(): Observable<any> {
        return this._http.get(Global.BASE_REQ_ENDPOINT)
            .map((response: Response) => <any>response.json())
            .catch(this.handleError);
    }

    getResults(answers: Answer[]): Observable<any> {
        let body = JSON.stringify(answers);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this._http.post(Global.BASE_REQ_RESULT_ENDPOINT, body, options)
            .map((response: Response) => <any>response.json())
            .catch(this.handleError);
    }

    uploadFile(formData) {
        return this._http.post(Global.BASE_REQ_UPLOAD_ENDPOINT, formData)
            .map((response: Response) => <any>response.json())
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}