﻿export class Requirement{
    Id: number;
    Body: string;
    IsFunctional: boolean;
}