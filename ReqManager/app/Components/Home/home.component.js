"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
// Services
var requirement_service_1 = require("../../Services/requirement.service");
var HomeComponent = (function () {
    function HomeComponent(_requirementsService, elem) {
        this._requirementsService = _requirementsService;
        this.elem = elem;
        this.functionalContainerId = "functional-container";
        this.nonFunctionalContainerId = "non-functional-container";
        this.requirementBlockClass = "requirement-block";
        this.bigScoreColor = "green";
        this.smallScoreColor = "red";
        this.answers = [];
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent.prototype.loadRequirements = function () {
        var _this = this;
        this._requirementsService.getRequirements()
            .subscribe(function (requirements) {
            _this.requirements = requirements;
        });
    };
    HomeComponent.prototype.calculateResults = function () {
        var _this = this;
        this._requirementsService.getResults(this.answers)
            .subscribe(function (score) {
            _this.score = score;
        });
    };
    HomeComponent.prototype.handleScoreStyle = function () {
        if (this.score < 50) {
            return this.smallScoreColor;
        }
        else {
            return this.bigScoreColor;
        }
    };
    HomeComponent.prototype.allowDrop = function (ev) {
        ev.preventDefault();
    };
    HomeComponent.prototype.drag = function (ev) {
        ev.dataTransfer.setData("text", ev.target.id);
    };
    HomeComponent.prototype.drop = function (ev) {
        ev.preventDefault();
        var data = ev.dataTransfer.getData("text");
        var requirementsBlock = document.getElementById(data);
        var requirementsContainer = null;
        if (ev.target.getAttribute("class") === this.requirementBlockClass) {
            requirementsContainer = ev.target.parentNode;
        }
        else {
            requirementsContainer = ev.target;
        }
        requirementsContainer.appendChild(requirementsBlock);
        requirementsBlock.setAttribute("draggable", "false");
        var isReqFunctional = false;
        if (requirementsContainer.id === this.functionalContainerId) {
            isReqFunctional = true;
        }
        else if (requirementsContainer.id === this.nonFunctionalContainerId) {
            isReqFunctional = false;
        }
        var answer = {
            RequirementId: data,
            IsFunctional: isReqFunctional
        };
        this.answers.push(answer);
    };
    HomeComponent.prototype.uploadFile = function () {
        var _this = this;
        this.resetAll();
        var files = this.elem.nativeElement.querySelector("#selectFile").files;
        var formData = new FormData();
        var file = files[0];
        formData.append('uploadFile', file, file.name);
        this._requirementsService.uploadFile(formData)
            .subscribe(function (res) { _this.requirements = res; });
    };
    HomeComponent.prototype.resetAll = function () {
        this.answers = [];
        this.requirements = [];
        var heading = document.createElement("h3");
        heading.style.fontFamily = "Franklin Gothic Medium";
        heading.style.fontSize = "16px";
        heading.style.textAlign = "center";
        heading.style.margin = "0";
        heading.style.marginBottom = "7px";
        var heading2 = document.createElement("h3");
        heading2.style.fontFamily = "Franklin Gothic Medium";
        heading2.style.fontSize = "16px";
        heading2.style.textAlign = "center";
        heading2.style.margin = "0";
        heading2.style.marginBottom = "7px";
        heading.innerHTML = "Функціональні вимоги";
        document.getElementById(this.functionalContainerId).innerHTML = "";
        document.getElementById(this.functionalContainerId).appendChild(heading);
        heading2.innerHTML = "Нефункціональні вимоги";
        document.getElementById(this.nonFunctionalContainerId).innerHTML = "";
        document.getElementById(this.nonFunctionalContainerId).appendChild(heading2);
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: 'home.component.html',
        styleUrls: ['home.component.css']
    }),
    __metadata("design:paramtypes", [requirement_service_1.RequirementService,
        core_1.ElementRef])
], HomeComponent);
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map