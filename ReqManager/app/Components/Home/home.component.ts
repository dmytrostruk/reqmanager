﻿import { Component, OnInit, ElementRef } from "@angular/core";
import { Requirement } from '../../Models/requirement';
import { Answer } from '../../Models/answer';

// Services
import { RequirementService } from '../../Services/requirement.service';

@Component({
    moduleId: module.id,
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.css']
})

export class HomeComponent {

    functionalContainerId: string = "functional-container";
    nonFunctionalContainerId: string = "non-functional-container";
    requirementBlockClass: string = "requirement-block";
    bigScoreColor: string = "green";
    smallScoreColor: string = "red";

    requirements: Requirement[];
    answers: Answer[] = [];
    score: number;

    constructor(
        private _requirementsService: RequirementService,
        private elem: ElementRef
    ) { }

    ngOnInit() {
        
    }

    loadRequirements() {
        this._requirementsService.getRequirements()
            .subscribe(requirements => {
                this.requirements = requirements;
            });
    }

    calculateResults() {
        this._requirementsService.getResults(this.answers)
            .subscribe(score => {
                this.score = score;
            });
    }

    handleScoreStyle() {
        if (this.score < 50) {
            return this.smallScoreColor;
        }
        else {
            return this.bigScoreColor;
        }
    }

    allowDrop(ev) {
        ev.preventDefault();
    }3

    drag(ev) {
        ev.dataTransfer.setData("text", ev.target.id);
    }

    drop(ev) {
        ev.preventDefault();

        let data = ev.dataTransfer.getData("text");
        let requirementsBlock = document.getElementById(data);
        let requirementsContainer = null;

        if (ev.target.getAttribute("class") === this.requirementBlockClass) {
            requirementsContainer = ev.target.parentNode;
        }
        else {
            requirementsContainer = ev.target;
        }

        requirementsContainer.appendChild(requirementsBlock);
        requirementsBlock.setAttribute("draggable", "false");

        let isReqFunctional = false; 

        if (requirementsContainer.id === this.functionalContainerId) {
            isReqFunctional = true;
        }
        else if (requirementsContainer.id === this.nonFunctionalContainerId) {
            isReqFunctional = false;
        }

        let answer: Answer = {
            RequirementId: data,
            IsFunctional: isReqFunctional
        };

        this.answers.push(answer);
    }

    uploadFile() {
        this.resetAll();
        let files = this.elem.nativeElement.querySelector("#selectFile").files;
        let formData = new FormData();
        let file = files[0];
        formData.append('uploadFile', file, file.name);
        this._requirementsService.uploadFile(formData)
            .subscribe(res => { this.requirements = res;});
    }

    resetAll() {
        this.answers = [];
        this.requirements = [];
        var heading = document.createElement("h3");
        heading.style.fontFamily = "Franklin Gothic Medium";
        heading.style.fontSize = "16px";
        heading.style.textAlign = "center";
        heading.style.margin = "0";
        heading.style.marginBottom = "7px";

        var heading2 = document.createElement("h3");
        heading2.style.fontFamily = "Franklin Gothic Medium";
        heading2.style.fontSize = "16px";
        heading2.style.textAlign = "center";
        heading2.style.margin = "0";
        heading2.style.marginBottom = "7px";

        heading.innerHTML = "Функціональні вимоги";
        document.getElementById(this.functionalContainerId).innerHTML = "";
        document.getElementById(this.functionalContainerId).appendChild(heading);

        heading2.innerHTML = "Нефункціональні вимоги"
        document.getElementById(this.nonFunctionalContainerId).innerHTML = "";
        document.getElementById(this.nonFunctionalContainerId).appendChild(heading2);
    }
}