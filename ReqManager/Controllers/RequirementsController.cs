﻿using ReqManager.Common.Interfaces;
using ReqManager.Common.Models;
using ReqManager.Parsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace ReqManager.Controllers
{
    public class RequirementsController : BaseController
    {
        private readonly IRequirementService _requirementService;

        public RequirementsController(IRequirementService requirementService)
        {
            _requirementService = requirementService;
        }

        public HttpResponseMessage Get()
        {
            return ToJson(_requirementService.GetRequirements());
        }

        [Route("api/requirements/result")]
        public HttpResponseMessage Result(ICollection<Answer> answers)
        {
            return ToJson(_requirementService.GetScore(answers));
        }

        [Route("api/requirements/uploadfile")]
        public HttpResponseMessage UploadFile()
        {
            try
            {
                var requirements = new List<Requirement>();

                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        var filePath = HttpContext.Current.Server.MapPath("~/" + postedFile.FileName);
                        postedFile.SaveAs(filePath);

                        var manager = new ParsingManager();
                        var reqs = manager.GetRequirements(filePath);
                        var funcReqs = reqs.Item1;
                        var nonFuncReqs = reqs.Item2;
                        int i = 0;
                        foreach(var req in funcReqs)
                        {
                            i++;
                            requirements.Add(new Requirement()
                            {
                                Id = i,
                                Body = req,
                                IsFunctional = true
                            });
                        }
                        foreach (var req in nonFuncReqs)
                        {
                            i++;
                            requirements.Add(new Requirement()
                            {
                                Id = i,
                                Body = req,
                                IsFunctional = false
                            });
                        }

                        Random rnd = new Random();
                        requirements = requirements.OrderBy(x => rnd.Next()).ToList();
                        _requirementService.SaveRequirements(requirements);
                        return ToJson(requirements);
                    }
                }
            }
            catch(Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}