﻿using ReqManager.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReqManager.Common.Interfaces
{
    public interface IRequirementService
    {
        ICollection<Requirement> GetRequirements();
        void SaveRequirements(ICollection<Requirement> reqs);
        float GetScore(ICollection<Answer> answers);
    }
}
