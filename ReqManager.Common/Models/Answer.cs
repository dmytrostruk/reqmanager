﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReqManager.Common.Models
{
    public class Answer
    {
        public int RequirementId { get; set; }
        public bool IsFunctional { get; set; }
    }
}
