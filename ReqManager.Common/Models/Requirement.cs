﻿namespace ReqManager.Common.Models
{
    public class Requirement
    {
        public int Id { get; set; }
        public string Body { get; set; }
        public bool IsFunctional { get; set; }
    }
}
