﻿using ReqManager.Common.Interfaces;
using System;
using System.Collections.Generic;
using ReqManager.Common.Models;
using System.Linq;

namespace ReqManager.Common.Services
{
    public class RequirementService : IRequirementService
    {
        private ICollection<Requirement> etalonReqs;
        public ICollection<Requirement> GetRequirements()
        {
            var requirements = new List<Requirement>();

            // TODO: Hardcoded data. Need to add special logic for this.
            requirements.Add(new Requirement()
            {
                Id = 1,
                Body = "1.1 Система повинна опрацьовувати файли, не більше ніж 1Гб.",
                IsFunctional = true
            });

            requirements.Add(new Requirement()
            {
                Id = 2,
                Body = "1.2 В заголовку сторінки повинний відображатись логотип системи.",
                IsFunctional = false
            });

            requirements.Add(new Requirement()
            {
                Id = 3,
                Body = "1.3 Програма повинна працювати з файлами розширення .docx та .pdf",
                IsFunctional = true
            });

            requirements.Add(new Requirement()
            {
                Id = 4,
                Body = "1.4 Програма повинна працювати паралельно з 10 і більше користувачами.",
                IsFunctional = false
            });

            requirements.Add(new Requirement()
            {
                Id = 5,
                Body = "1.5 Повинні бути реалізовані всі CRUD операції для тестів.",
                IsFunctional = true
            });

            requirements.Add(new Requirement()
            {
                Id = 6,
                Body = "1.6 Програма повинна працювати з некоректними даними",
                IsFunctional = true
            });

            return requirements;
        }

        public float GetScore(ICollection<Answer> answers)
        {
            var requirements = etalonReqs;
            float scoreForAnswer = (float)100 / requirements.Count;

            float userScore = 0;

            foreach(var answer in answers)
            {
                var requirement = requirements.SingleOrDefault(r => r.Id == answer.RequirementId);

                if(requirement != null)
                {
                    userScore += answer.IsFunctional == requirement.IsFunctional ? scoreForAnswer : 0;
                }
            }

            return userScore;
        }

        public void SaveRequirements(ICollection<Requirement> reqs)
        {
            etalonReqs = reqs;
        }
    }
}